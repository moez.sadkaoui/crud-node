const express = require('express');
const bodyParser = require('body-parser');
const rfp = require('./routes/rfp.route'); // Imports routes for the products
const company = require('./routes/company.route');
var cors = require('cors')
const app = express();


const connection = require("../mini-projet/db")
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/rfp', rfp);
app.use('/company',company)
let port = 1234;
app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});