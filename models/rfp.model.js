const mongoose = require('mongoose');
const Company = require('./company.model')
const Schema = mongoose.Schema;

let RfpSchema = new Schema({
    name: String,
            duration: Number,
            price: Number,
            company: { type: Schema.Types.ObjectId, ref: 'Company' }
});


// Export the model
module.exports = mongoose.model('Rfp', RfpSchema);