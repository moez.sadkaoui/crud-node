const Rfp = require('../models/rfp.model');
const Company = require('../models/company.model')
var mongoose = require('mongoose');
//create
exports.rfp_create = function (req, res) {


     
        let rfp = new Rfp(
            {
                name: req.body.name,
                duration: req.body.duration,
                price: req.body.price,
                
                company:mongoose.Types.ObjectId(req.body.id)
            }
        );
   
   
  
   
    rfp.save(function (err) {
        if (err) {
            console.log(err);
        }
        res.send('Product Created successfully')
    })
};

//read

exports.rfp_details = function (req, res) {
    Rfp.findById(req.params.id, function (err, rfp) {
        if (err)
         console.log(err);
      
        res.send(rfp);
    })
};


exports.rfp_getAll = function (req, res) {
    Rfp.find( function (err, rfp) {
        if (err)
         console.log(err);
        res.send(rfp);
    })
};

//put

exports.rfp_update = function (req, res) {
    Rfp.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, product) {
        if (err) return next(err);
        res.send('Product udpated.');
    });
};

exports.rfp_delete = function (req, res) {
    Rfp.findByIdAndDelete(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};


