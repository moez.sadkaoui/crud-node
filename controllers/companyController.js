const Company = require('../models/company.model');

//create
exports.company_create = function (req, res) {
    let company = new Company(
        {
            companyName: req.body.companyName,
            location: req.body.location
        }
    );

    company.save(function (err) {
        if (err) {
            console.log(err);
        }
        res.send('Company Created successfully')
    })
};

//read

exports.company_details = function (req, res) {
    Company.findById(req.params.id, function (err, company) {
        if (err)
         console.log(err);
         console.log(company)
        res.send(company);
    })
};


exports.company_getAll = function (req, res) {
    Company.find( function (err, company) {
        if (err)
         console.log(err);
        res.send(company);
    })
};

//put

exports.company_update = function (req, res) {
    Company.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, product) {
        if (err) return next(err);
        res.send('Company udpated.');
    });
};

exports.company_delete = function (req, res) {
    Company.findByIdAndDelete(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};


