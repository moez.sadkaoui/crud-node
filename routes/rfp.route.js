const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const rfp_controller = require('../controllers/rfp.controller');

router.post('/create', rfp_controller.rfp_create);

router.get('/get-all', rfp_controller.rfp_getAll);

router.get('/:id', rfp_controller.rfp_details);

router.put('/:id/update', rfp_controller.rfp_update);

router.delete('/:id/delete', rfp_controller.rfp_delete);


module.exports = router;