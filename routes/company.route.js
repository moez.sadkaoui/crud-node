const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const company_controller = require('../controllers/companyController');

router.post('/create', company_controller.company_create);

router.get('/get-all', company_controller.company_getAll);

router.get('/:id', company_controller.company_details);

router.put('/:id/update', company_controller.company_update);

router.delete('/:id/delete', company_controller.company_delete);


module.exports = router;